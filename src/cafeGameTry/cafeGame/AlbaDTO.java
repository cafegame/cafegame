package cafeGameTry.cafeGame;

public abstract class AlbaDTO {

	/* 필드 */	
	public int mission = 10000*((int)(Math.random()*3)+2);
    protected int income = 0;
	protected int complaint = ((int)(Math.random()*10)+1);
	

	/* 기본 생성자 */
	public AlbaDTO() {
		super();
	}
	
	/* 매개변수가 있는 생성자 */
	public AlbaDTO(int mission, int income, int complaint) {
		super();
		this.mission = mission;
		this.income = income;
		this.complaint = complaint;
	}

	
	
	/* getter / setter */
	public int getMission() {
		return mission;
	}

	public int getIncome() {
		return income;
	}
	
	public int getComplaint() {
		return complaint;
	}
	
	public void setMission(int mission) {
		this.mission = mission;
	}
	
	public void setIncome(int income) {
		this.income = income;
	}
	
	public void setComplaint(int complaint) {
		this.complaint = complaint;
	}
	
	
	
	
	
	
	public abstract void work();
	

	public abstract void AlbaDTO();
	
}

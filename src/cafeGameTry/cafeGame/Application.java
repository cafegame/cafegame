package cafeGameTry.cafeGame;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		
			Scanner sc = new Scanner(System.in);
			
			System.out.println("[system] : 여기는 현'cafe! 오늘 업무 평가를 마지막으로 각 파트의 매니저 진급이 결정된다!"); 
			System.out.println();
			System.out.println("[system] : 우리 카페에는 매니저가 되기를 바라는 알바생 세 명이 있습니다.");  
			System.out.println("[system] : 1. 바리스타 현우 2. 파티쉐 이현 3. 플로어 현재");                                                                                                       
			System.out.print("[system] : 당신이 고른 알바생에 해당하는 숫자를 입력하세요! : "); 
			
			int albaSelect = sc.nextInt();
			sc.nextLine();
			String alba = "";
			switch (albaSelect) {
			case 1 : 
				alba = "바리스타 현우";
				Barista bari = new Barista();
				System.out.println();
				System.out.println("[system] : 가봅시다! " + bari.mission + "원을 벌어서 " + alba +"의 진급 도전!");
				while (true) {
					
					System.out.println("[system] : 어서 업무를 시작하세요! ");               
					System.out.println("[system] : 1. 넵!! 2. 못하겠어요...");                    
					System.out.print("[system] : 당신의 선택은? : "); 
					
					int num = sc.nextInt();
					System.out.println();
					switch (num) {
						case 1 : bari.work(); break;
						case 2: System.out.println("[system] : 오늘은 일단 퇴근합니다...."); return;
						default : System.out.println("[system] : 둘 중 하나를 선택하세요 ."); break;
						}
				}
			case 2 : 
				alba = "파티쉐 이현";
				Patissier pati = new Patissier();
				System.out.println();
				System.out.println("[system] : 가봅시다! " + pati.mission + "원을 벌어서 " + alba +"의 진급 도전!");
				while (true) {
					
					System.out.println("[system] : 어서 업무를 시작하세요! ");               
					System.out.println("[system] : 1. 넵!! 2. 못하겠어요...");                    
					System.out.print("[system] : 당신의 선택은? : "); 
					int num = sc.nextInt();
					System.out.println();
					switch (num) {
						case 1 : pati.work(); break;
						case 2: System.out.println("[system] : 오늘은 일단 퇴근합니다...."); return;
						default : System.out.println("[system] : 둘 중 하나를 선택하세요 ."); break;
						}
				}	
			case 3 : 
				alba = "플로어 현재";
				Floor flo = new Floor();
				System.out.println();
				System.out.println("[system] : 가봅시다! " + flo.mission + "원을 벌어서 " + alba +"의 진급 도전!");
				while (true) {
					
					System.out.println("[system] : 어서 업무를 시작하세요! ");               
					System.out.println("[system] : 1. 넵!! 2. 못하겠어요...");                    
					System.out.print("[system] : 당신의 선택은? : "); 
					int num = sc.nextInt();
					System.out.println();
					switch (num) {
						case 1 : flo.work(); break;
						case 2: System.out.println("[system] : 오늘은 일단 퇴근합니다...."); return;
						default : System.out.println("[system] : 둘 중 하나를 선택하세요 ."); break;
						}
				}	
				
			}
			
	}
}

